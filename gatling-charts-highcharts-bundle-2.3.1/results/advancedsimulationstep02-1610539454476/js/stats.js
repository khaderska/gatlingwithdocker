var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "126",
        "ok": "126",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "97",
        "ok": "97",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles4": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 126,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "5.727",
        "ok": "5.727",
        "ko": "-"
    }
},
contents: {
"req_home-8cf04": {
        type: "REQUEST",
        name: "Home",
path: "Home",
pathFormatted: "req_home-8cf04",
stats: {
    "name": "Home",
    "numberOfRequests": {
        "total": "24",
        "ok": "24",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "134",
        "ok": "134",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles1": {
        "total": "128",
        "ok": "128",
        "ko": "-"
    },
    "percentiles2": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles3": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 24,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.091",
        "ok": "1.091",
        "ko": "-"
    }
}
    },"req_home-redirect-1-df14c": {
        type: "REQUEST",
        name: "Home Redirect 1",
path: "Home Redirect 1",
pathFormatted: "req_home-redirect-1-df14c",
stats: {
    "name": "Home Redirect 1",
    "numberOfRequests": {
        "total": "24",
        "ok": "24",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "92",
        "ok": "92",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles3": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "percentiles4": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 24,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1.091",
        "ok": "1.091",
        "ko": "-"
    }
}
    },"req_search-13348": {
        type: "REQUEST",
        name: "Search",
path: "Search",
pathFormatted: "req_search-13348",
stats: {
    "name": "Search",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles2": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles3": {
        "total": "92",
        "ok": "92",
        "ko": "-"
    },
    "percentiles4": {
        "total": "94",
        "ok": "94",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_select-e0626": {
        type: "REQUEST",
        name: "Select",
path: "Select",
pathFormatted: "req_select-e0626",
stats: {
    "name": "Select",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles3": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles4": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_page-1-dffde": {
        type: "REQUEST",
        name: "Page 1",
path: "Page 1",
pathFormatted: "req_page-1-dffde",
stats: {
    "name": "Page 1",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles4": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_page-2-9cd0e": {
        type: "REQUEST",
        name: "Page 2",
path: "Page 2",
pathFormatted: "req_page-2-9cd0e",
stats: {
    "name": "Page 2",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles4": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_page-3-cdc73": {
        type: "REQUEST",
        name: "Page 3",
path: "Page 3",
pathFormatted: "req_page-3-cdc73",
stats: {
    "name": "Page 3",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles4": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_page-4-badfc": {
        type: "REQUEST",
        name: "Page 4",
path: "Page 4",
pathFormatted: "req_page-4-badfc",
stats: {
    "name": "Page 4",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles4": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 12,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.545",
        "ok": "0.545",
        "ko": "-"
    }
}
    },"req_form-d359c": {
        type: "REQUEST",
        name: "Form",
path: "Form",
pathFormatted: "req_form-d359c",
stats: {
    "name": "Form",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles2": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles3": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles4": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_post-03d94": {
        type: "REQUEST",
        name: "Post",
path: "Post",
pathFormatted: "req_post-03d94",
stats: {
    "name": "Post",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles2": {
        "total": "90",
        "ok": "90",
        "ko": "-"
    },
    "percentiles3": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "percentiles4": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    },"req_post-redirect-1-1713a": {
        type: "REQUEST",
        name: "Post Redirect 1",
path: "Post Redirect 1",
pathFormatted: "req_post-redirect-1-1713a",
stats: {
    "name": "Post Redirect 1",
    "numberOfRequests": {
        "total": "2",
        "ok": "2",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "percentiles2": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "percentiles3": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "percentiles4": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 2,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.091",
        "ok": "0.091",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
