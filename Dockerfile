FROM java:8-jdk-alpine
ADD gatling-charts-highcharts-bundle-2.3.1 /gatling
WORKDIR /gatling/bin
CMD ["./gatling.sh", "-s", "computerdatabase.BasicSimulation"]
